/**
*
* CommitListItem
*
*/

import React from 'react';
// import styled from 'styled-components';


function CommitListItem({ item }) {
  const { name, url } = item;
  return (
    <li>
      <a href={url} target="_blank">{name}</a>
    </li>
  );
}

CommitListItem.propTypes = {
  item: React.PropTypes.shape({
    name: React.PropTypes.string.isRequired,
    url: React.PropTypes.string.isRequired,
  }).isRequired,
};

export default CommitListItem;
