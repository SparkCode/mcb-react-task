/**
*
* Search
*
*/

import React, { PropTypes } from 'react';
// import styled from 'styled-components';


function Search({ searchText, setSearchText, onSearch }) {
  return (
    <div>
      <input type="text" value={searchText} onChange={(event) => setSearchText(event.target.value)} />
      <button onClick={() => onSearch(searchText)}>Search</button>
    </div>
  );
}

Search.propTypes = {
  searchText: PropTypes.string,
  setSearchText: PropTypes.func.isRequired,
  onSearch: PropTypes.func,
};

Search.defaultProps = {
  onSearch: () => {},
};

export default Search;
