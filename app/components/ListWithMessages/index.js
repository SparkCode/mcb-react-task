/**
*
* ListWithMessages
*
*/

import React from 'react';
import List from '../List';
// import styled from 'styled-components';


function ListWithMessages({ ListItem, items, loading, error, onLoadingMessage, onErrorMessage, defaultMessage, ...props }) {
  if (items) {
    return (
      <List ListItem={ListItem} items={items} {...props} />
    );
  }
  const message = loading ? onLoadingMessage : error ? onErrorMessage : defaultMessage;

  return (
    <div>{message}</div>
  );
}

ListWithMessages.propTypes = {
  ListItem: React.PropTypes.func.isRequired,
  items: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
  loading: React.PropTypes.bool.isRequired,
  error: React.PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
  onLoadingMessage: React.PropTypes.string,
  onErrorMessage: React.PropTypes.string,
  defaultMessage: React.PropTypes.string,
};

ListWithMessages.defaultProps = {
  onLoadingMessage: 'Loading..',
  onErrorMessage: 'Something went wrong..',
  defaultMessage: '',
}

export default ListWithMessages;
