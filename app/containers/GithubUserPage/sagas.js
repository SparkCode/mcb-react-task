import loadReposWatcher from '../GithubRepoList/sagas';
import githubCommitWatcher from '../GithubCommitList/sagas';

// All sagas to be loaded
export default [
  loadReposWatcher, githubCommitWatcher,
];
