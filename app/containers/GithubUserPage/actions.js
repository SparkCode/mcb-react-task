/*
 *
 * GithubUserPage actions
 *
 */

import { createAction } from 'redux-act';

export const invalidateUserRepos = createAction('Invalidate user repositories');
export const loadUserRepos = createAction('Load user repositories', (userName) => ({ userName }));
export const userReposLoaded = createAction('User repositories loaded');
export const userReposLoadingError = createAction('loading user repositories fail');

export const invalidateRepoCommits = createAction('Invalidate repository commits');
export const loadRepoCommits = createAction('Load repository commits', (userName, repoName) => ({ userName, repoName }));
export const repoCommitsLoaded = createAction('Repository commits loaded');
export const repoCommitsLoadingError = createAction('loading repository commits fail');

