/**
 * Direct selector to the githubCommitList state domain
 */
import { createSelector } from 'reselect';

const selectGithubCommitListDomain = () => (state) => state.get('GithubUserPage').repoCommits;

/**
 * Other specific selectors
 */

const makeSelectLoading = () => createSelector(
  selectGithubCommitListDomain(),
  (substate) => substate.get('loading')
);

const makeSelectRepoCommits = () => createSelector(
  selectGithubCommitListDomain(),
  (substate) => substate.get('data')
);

const makeSelectError = () => createSelector(
  selectGithubCommitListDomain(),
  (substate) => substate.get('error')
);


/**
 * Default selector used by GithubCommitList
 */

const makeSelectGithubCommitList = () => createSelector(
  selectGithubCommitListDomain(),
  (substate) => substate.toJS()
);

export default makeSelectGithubCommitList;
export {
  selectGithubCommitListDomain,
  makeSelectLoading,
  makeSelectRepoCommits,
  makeSelectError,
};
