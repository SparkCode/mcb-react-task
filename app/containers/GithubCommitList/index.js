/*
 *
 * GithubCommitList
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectError, makeSelectLoading, makeSelectRepoCommits } from './selectors';
import CommitListItem from '../../components/CommitListItem';
import ListWithMessages from '../../components/ListWithMessages';

export function GithubCommitList({ repoCommits, loading, error }) {
  const items = repoCommits && repoCommits.map((commit) => ({ id: commit.sha, name: commit.commit.message, url: commit.html_url }));
  return (<ListWithMessages loading={loading} ListItem={CommitListItem} error={error} items={items} defaultMessage={'Click on repo to see commits'} />);
}

GithubCommitList.propTypes = {
  repoCommits: PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  repoCommits: makeSelectRepoCommits(),
  error: makeSelectError(),
});

export default connect(mapStateToProps)(GithubCommitList);
