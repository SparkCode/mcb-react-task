/*
 *
 * UserReposSearch
 *
 */

import { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withState } from 'recompose';

import Search from '../../components/Search';
import * as actions from '../GithubUserPage/actions';

const UserReposSearch = withState('searchText', 'setSearchText', 'giko')(Search);

UserReposSearch.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    onSearch: (userName) => {
      dispatch(actions.invalidateRepoCommits());
      dispatch(actions.invalidateUserRepos());
      dispatch(actions.loadUserRepos(userName));
    },
  };
}
export default connect(null, mapDispatchToProps)(UserReposSearch);
