import { take, call, put } from 'redux-saga/effects';
import { request } from 'utils/request';

import * as actions from '../GithubUserPage/actions';

// Individual exports for testing
export function* loadReposSaga(action) {
  try {
    const result = yield call(request, `users/${action.payload.userName}/repos`);
    yield put(actions.userReposLoaded(result));
  } catch (e) {
    yield put(actions.userReposLoadingError(e));
  }
}

export function* loadReposWatcher() {
  while (true) {
    const action = yield take(actions.loadUserRepos.getType());
    yield call(loadReposSaga, action);
  }
}

export default loadReposWatcher;
