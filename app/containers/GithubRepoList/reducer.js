/*
 *
 * GithubRepoList reducer
 *
 */

import { fromJS } from 'immutable';
import { createReducer } from 'redux-act';

import * as actions from '../GithubUserPage/actions';

const initState = fromJS({
  loading: false,
  error: false,
  data: false,
});

const githubRepoListReducer = createReducer((on) => {
  on(actions.invalidateUserRepos, (state) => state
    .set('data', false));
  on(actions.loadUserRepos, (state) => state
    .set('loading', true)
    .set('error', false));
  on(actions.userReposLoaded, (state, payload) => state
    .set('data', payload)
    .set('loading', false));
  on(actions.userReposLoadingError, (state, payload) => state
    .set('error', payload)
    .set('loading', false));
}, initState);

export default githubRepoListReducer;
