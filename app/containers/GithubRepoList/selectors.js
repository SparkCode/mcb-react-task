import { createSelector } from 'reselect';

/**
 * Direct selector to the githubUserContainer state domain
 */
const selectGithubUserContainerDomain = () => (state) => state.get('GithubUserPage').userRepos;

/**
 * Other specific selectors
 */

const makeSelectUserRepos = () => createSelector(
  selectGithubUserContainerDomain(),
  (substate) => substate.get('data')
);

const makeSelectLoading = () => createSelector(
  selectGithubUserContainerDomain(),
  (substate) => substate.get('loading')
);

const makeSelectError = () => createSelector(
  selectGithubUserContainerDomain(),
  (substate) => substate.get('error')
);


/**
 * Default selector used by GithubUserPage
 */

const makeSelectGithubUserContainer = () => createSelector(
  selectGithubUserContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectGithubUserContainer;
export {
  selectGithubUserContainerDomain,
  makeSelectUserRepos,
  makeSelectLoading,
  makeSelectError,
};
