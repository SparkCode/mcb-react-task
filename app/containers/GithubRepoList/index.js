/*
 *
 * GithubUserContainer
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectError, makeSelectLoading, makeSelectUserRepos } from './selectors';
import RepoListItem from '../../components/RepoListItem';
import ListWithMessages from '../../components/ListWithMessages';
import {invalidateRepoCommits, loadRepoCommits} from '../GithubUserPage/actions';

export class GithubUserContainer extends React.PureComponent {
  onRepoClick = (repoId) => {
    const { onRepoClick } = this.props;
    const pos = this.items.findIndex((repo) => repo.id === repoId);
    const { userName, name } = this.items[pos];
    onRepoClick(name, userName);
  };

  render() {
    const { repositories, loading, error } = this.props;
    this.items = repositories && repositories.map((repo) => ({ id: repo.id.toString(), name: repo.name, userName: repo.owner.login }));
    return (
      <div>
        <ListWithMessages loading={loading} ListItem={RepoListItem} error={error} items={this.items} defaultMessage={'Type something...'} onItemClick={this.onRepoClick} />
      </div>
    );
  }
}

GithubUserContainer.propTypes = {
  repositories: PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
  onRepoClick: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.bool,
  ]),
};

const mapStateToProps = createStructuredSelector({
  repositories: makeSelectUserRepos(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

function mapDispatchToProps(dispatch) {
  return {
    onRepoClick: (repoName, userName) => {
      dispatch(invalidateRepoCommits(userName, repoName));
      dispatch(loadRepoCommits(userName, repoName));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GithubUserContainer);
